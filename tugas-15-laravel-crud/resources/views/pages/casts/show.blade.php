@extends('layouts.main')
@section('content')
  <a href="{{ url('/cast/create') }}" class="btn btn-primary">Create</a>
  <table class="table table-striped">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
      <tr id="{{ $cast->id }}">
        <th scope="row">1</th>
        <td>{{ $cast->nama }}</td>
        <td>{{ $cast->umur }}</td>
        <td>{{ $cast->bio }}</td>
      </tr>
    </tbody>
  </table>
@endsection