@extends('layouts.main')
@section('content')
  <form action="/cast" method="post">
    @csrf
    <div class="form-group">
      <label for="nama">Nama</label>
      <input 
        id="nama" 
        name="nama"
        type="text" 
        class="form-control" 
        placeholder="Sabiq Muhammad"
      />
    </div>
    <div class="form-group">
      <label for="umur">Umur</label>
      <input 
        id="umur" 
        name="umur"
        type="number" 
        class="form-control" 
        placeholder="19"
      />
    </div>
    <div class="form-group">
      <label for="bio">Bio</label>
      <textarea 
        id="bio" 
        name="bio"
        class="form-control" 
        rows="3">
      </textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection