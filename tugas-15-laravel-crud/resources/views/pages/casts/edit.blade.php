@extends('layouts.main')
@section('content')
  <form action="/cast/{{ $cast->id }}" method="post">
    {{ csrf_field() }}
    {{ method_field('PUT') }}
    <div class="form-group">
      <label for="nama">Nama</label>
      <input 
        id="nama" 
        name="nama"
        type="text" 
        class="form-control" 
        placeholder="Sabiq Muhammad"
        value="{{ $cast->nama }}"
      />
    </div>
    <div class="form-group">
      <label for="umur">Umur</label>
      <input 
        id="umur" 
        name="umur"
        type="number" 
        class="form-control" 
        placeholder="19"
        value="{{ $cast->umur }}"
      />
    </div>
    <div class="form-group">
      <label for="bio">Bio</label>
      <textarea 
        id="bio" 
        name="bio"
        class="form-control" 
        rows="3">
        {{ $cast->bio }}
      </textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection