<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataController extends Controller
{
    public function index()
    {
        $data = [
            'title' => 'Table'
        ];
        return view('pages.table', $data);
    }

    public function showData()
    {
        $data = [
            'title' => 'Data Table'
        ];
        return view('pages.data-table', $data);
    }
}
