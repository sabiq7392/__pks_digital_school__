<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MasterController extends Controller
{
    public function index() 
    {
        $data = [
            'title' => 'No Data'
        ];
        return view('layouts.main', $data);
    }
}
