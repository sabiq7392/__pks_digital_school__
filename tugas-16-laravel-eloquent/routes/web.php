<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/main', 'MasterController@index');
// Route::get('/table', 'DataController@index');
// Route::get('/data-table', 'DataController@showData');

Route::resource('cast', 'CastController');
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/create', 'CastController@create');
// Route::get('/cast/{id}', 'CastController@show');
// Route::get('/cast/{id}/edit', 'CastController@edit');
// Route::post('/cast', 'CastController@store');
// Route::put('/cast/{id}', 'CastController@update');
// Route::delete('/cast/{id}', 'CastController@destroy');
