import React from 'react';
import ReactDOM from 'react-dom';
import reactToWebComponent from 'react-to-webcomponent';
import PropTypes from 'prop-types';

class NavLink extends React.Component {
  render() {
    return (
      <a href={this.props.href} className="nav-link">
        <i className={`nav-icon ${this.props.icon}`}></i>
        <p>{this.props.content}</p>
      </a>
    );
  }
}

NavLink.defaultProps = {
  href: '#',
};

NavLink.propTypes = {
  content: PropTypes.string,
  href: PropTypes.string,
  icon: PropTypes.string,
};

customElements.define(
  'nav-link',
  reactToWebComponent(NavLink, React, ReactDOM)
);