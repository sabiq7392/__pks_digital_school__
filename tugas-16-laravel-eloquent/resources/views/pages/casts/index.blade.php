@extends('layouts.main')
@section('content')
  <a href="{{ url('/cast/create') }}" class="btn btn-primary">Create</a>
  <table class="table table-striped">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($casts as $cast)
        <tr id="{{ $cast->id }}">
          <th scope="row">{{ $loop->iteration }}</th>
          <td>{{ $cast->nama }}</td>
          <td>{{ $cast->umur }}</td>
          <td>{{ $cast->bio }}</td>
          <td>
            <a href="/cast/{{ $cast->id }}" class="btn btn-primary">Detail</a>
            <a href="/cast/{{ $cast->id }}/edit" class="btn btn-success">Edit</a>
            <form action="/cast/{{ $cast->id }}" method="POST">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <button type="submit" class="btn btn-danger">Delete</button>
            </form>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection
