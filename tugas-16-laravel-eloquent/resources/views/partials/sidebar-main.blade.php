<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <a href="../../index3.html" class="brand-link">
    <img src="{{ asset('admin/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE 3</span>
  </a>

  <div class="sidebar">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('admin/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="Sabiq Muhammad">
      </div>
      <div class="info">
        <a href="#" class="d-block">Sabiq Muhammad</a>
      </div>
    </div>

    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
          <nav-link 
            content="Dashboards"
            href="#"
            icon="fas fa-tachometer-alt">
          </nav-link>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-table"></i>
            <p>
              Tables
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul id="navTreeView" class="nav nav-treeview">
            <li class="nav-item">
              <nav-link
                content="Casts"
                href="{{ url('/cast') }}"
                icon="far fa-circle">
              </nav-link>
            </li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</aside>