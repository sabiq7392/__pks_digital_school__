<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cast;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
	public function index()
	{
		$data = [
			'title'=> 'Casts',
			'casts' => Cast::all(),
		];

		return view('pages.casts.index', $data);
	}

	public function create() 
	{
		$data = [
			'title' => 'Create Cast',
		];
		return view('pages.casts.create', $data);
	}

	public function store(Request $request) 
	{
		$request->validate([
			'nama' => 'required',
			'umur' => 'required',
			'bio' => 'required',
		]);

		Cast::create($request->all());

		return redirect('/cast');
	}

	public function show($id)
	{
		$data = [
			'title' => 'Cast',
			'cast' => Cast::find($id),
		];

		return view('pages.casts.show', $data);
	}

	public function edit($id)
	{
		$data = [
			'title' => 'Edit Cast',
			'cast' => Cast::find($id),
		];

		return view('pages.casts.edit', $data);
	}

	public function update(Request $request, $id) 
	{
		$cast = Cast::find($id);

		$request->validate([
			'nama' => 'required',
			'umur' => 'required',
			'bio' => 'required',
		]);

		$cast->update($request->all());

		return redirect("/cast/$id");
	}

	public function destroy($id)
	{
		$cast = Cast::find($id);

		$cast->delete();

		return redirect('/cast');
	}
}
