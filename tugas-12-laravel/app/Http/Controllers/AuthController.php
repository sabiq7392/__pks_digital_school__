<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class AuthController extends Controller
{
	public function index()
	{
		return view('register');
	}	

	public function welcome(Request $request) 
	{
		$username = [
			'username' => $request->input('first-name')
		];

		return view('welcome', $username);
	}
}
