<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Welcome | PKS-Tugas-1-HTML</title>
</head>
<body>
  <main>
    <header>
      <h1>SELAMAT DATANG, {{ $username }}</h1>
    </header>
    <section>
      <p>
        <b>
          Terima Kasih telah bergabung di Website Kami. 
          Media Belajar kita bersama
        </b>
      </p>
    </section>
  </main>
</body>
</html>